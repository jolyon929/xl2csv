// convert xls file into useful data

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS
#include <atlbase.h>
#include <iostream>
#include <fstream>
#include <codecvt>
#include <string>
#include <vector>

using namespace std;


#import "C:\Program Files\Microsoft Office\root\vfs\ProgramFilesCommonX64\Microsoft Shared\OFFICE16\MSO.DLL" \
    rename( "RGB", "MSORGB" )
using namespace Office;

#import "C:\Program Files\Microsoft Office\root\vfs\ProgramFilesCommonX86\Microsoft Shared\VBA\VBA6\VBE6EXT.OLB"
using namespace VBIDE;

#import "C:\Program Files\Microsoft Office\root\Office16\EXCEL.EXE" \
    rename( "DialogBox", "ExcelDialogBox" )                         \
    rename( "RGB", "ExcelRGB" )                                     \
    rename( "CopyFile", "ExcelCopyFile" )                           \
    rename( "ReplaceText", "ExcelReplaceText" )

Excel::_WorkbookPtr
get_workbook_ptr(const Excel::_ApplicationPtr& pApplication,
                 const wstring&         xl_file
                 );
vector<vector<wstring>>
get_excel_data(const Excel::RangePtr& pR);

void
create_csv_file(const wstring&                 XlFileName,
                const vector<vector<wstring>>& Spreadsheet
                );

int __cdecl
main(__in int                 argc,
     __in_ecount(argc) char** argv
     )
{
    HRESULT hr(E_INVALIDARG);

    if (argc < 2) {
        cout << "Please specify the excel file to convert" << endl;
        return hr;
    }
    USES_CONVERSION; // necessary

    if (SUCCEEDED(hr = CoInitialize(NULL))) {
        Excel::_ApplicationPtr pApplication;
        if (SUCCEEDED(hr = pApplication.CreateInstance(L"Excel.Application"))) {
            const wstring xl_file(A2W(argv[1]));
            if (Excel::_WorkbookPtr pW = get_workbook_ptr(pApplication, xl_file)) {
                if (Excel::_WorksheetPtr pS = pW->Sheets->Item[1]) {
                    if (Excel::RangePtr pR = pS->GetRange(_bstr_t(L"A1"), _bstr_t(L"Z1"))) {
                        vector<vector<wstring>> data_set = get_excel_data(pR);
                        create_csv_file(xl_file, data_set);
                    }
                }
                else {
                    cout << "Failed to open worksheet"  << endl;
                }
            }
            else {
                cout << "Failed to open" << argv[1] << endl;
            }
            pApplication->Quit();
        }
        CoUninitialize();
    }
    return hr;
}

Excel::_WorkbookPtr
get_workbook_ptr(const Excel::_ApplicationPtr& pApplication,
                 const wstring&         xl_file
                 )
{
    _variant_t varOption((long)DISP_E_PARAMNOTFOUND, VT_ERROR);
    Excel::_WorkbookPtr pW = pApplication->Workbooks->Open(_bstr_t(xl_file.c_str()),
                                                           varOption, varOption, varOption, varOption,
                                                           varOption, varOption, varOption, varOption,
                                                           varOption, varOption, varOption, varOption
                                                           );
    return pW; // implicit move
}

vector<vector<wstring>>
get_excel_data(const Excel::RangePtr& pR)
{
    vector<vector<wstring>> data_set;

    for (int r = 1;;++r) {
        vector<wstring> v;

        for (int c = 1;;++c) {
            _variant_t var = pR->Item[r][c];
            _bstr_t	   bstrText(var);

            if (bstrText.length() == 0) {
                data_set.push_back(v);
                break;
            }
            v.push_back((const wchar_t*) bstrText);
        }
        if (data_set[data_set.size() - 1].size() == 0) {
            break;
        }
    }
    return data_set; // implicit move
}

void
create_csv_file(const wstring&                 XlFileName,
                const vector<vector<wstring>>& Spreadsheet
                )
{
    wofstream ocsv;
    ocsv.imbue(locale(locale::empty(),
                     new codecvt_utf8<wchar_t, 0x10ffff, generate_header>
                     )
              ); /* utf8 csv is fine */

    USES_CONVERSION;
    string new_file(W2A(XlFileName.c_str()));

    ocsv.open(new_file += ".csv");
    if (ocsv.is_open()) {
        for(auto const& row : Spreadsheet) {
            for(auto const& col : row) {
                if(col != row.front())
                    ocsv << ", ";
                ocsv << "\"" << col.c_str() << "\"";
            }
            ocsv << endl;
        }
        ocsv.close();
    }
}
